<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'meteo',
    'name' => 'Тестовое задание на должность Junior PHP Developer',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'layout' => 'meteostations',
    'defaultRoute' => 'meteostations/index',
    'language' => 'ru_RU',
    'charset' => 'UTF-8',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'wU7IqE6P04R3i-ZA4ndj2I1vTkXGkOZX',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        //Настройка отображения URL
//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'rules' => [
//                //Правило отображения главной страницы
//                [
//                    //Шаблон ссылки
//                    'pattern' => '',
//                    //Маршрут для ссылки
//                    'route' => 'meteostations/index',
//                    //Суффикс, представляемый после шаблона ссылки
//                    'suffix' => ''
//                ],
//            ],
//        ],
        //Форматирование вывода времени и временной зоны http://php.net/manual/ru/timezones.php
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
            'timeZone' => 'Europe/Kiev',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
