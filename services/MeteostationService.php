<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\services;

use app\models\tables\Meteostation;
use app\models\tables\Location;
use Yii;

/**
 * Description of StationService
 *
 * @author box
 */
class MeteostationService
{
    /**
     * Создает и сохраняет метеостанцию 
     * @param app\models\forms\MeteostationFofm $form
     * @return mixed
     */
    public static function create($form)
    {
        $meteostation = new Meteostation;
        $meteostation->station_id = $form->station_id;
        $meteostation->type = $form->type;
        $transaction = Yii::$app->db->beginTransaction();
        if ($meteostation->save(false)) {
            $location = new Location;
            $location->name = $form->name;
            $location->latitude = $form->latitude;
            $location->longitude = $form->longitude;
            $location->meteostation_id = $meteostation->id;
            if ($location->save(false)) {
                $transaction->commit();
                return $meteostation;
            }
            $transaction->rollBack();
        }
        return false;
    }

    /**
     * Обновляет данные метеостанции
     * @param app\models\forms\MeteostationFofm $form
     * @param Meteostation $meteostation
     * @return mixed
     */
    public static function update($form, $meteostation)
    {
        $meteostation->type = $form->type;
        $transaction = Yii::$app->db->beginTransaction();
        if ($meteostation->save(false)) {
            $location = $meteostation->location;
            $location->latitude = $form->latitude;
            $location->longitude = $form->longitude;
            if ($location->save(false)) {
                $transaction->commit();
                return $meteostation;
            }
            $transaction->rollBack();
        }
        return false;
    }
}
