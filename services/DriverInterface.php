<?php

namespace app\services;
/**
 * Интерфейс для реализации библиотеки получения данных от метеодатчика
 * 
 * @author helvis
 */
interface DriverInterface
{
    /**
     * @abstract
     * @return MeteoData
     */
    public function getMeteoData();
}