<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\services;

use app\models\tables\MeteoData;

/**
 * Description of MeteoDataService
 *
 * @author helvis
 */
class MeteoDataService
{
    /**
     * @var type 
     */
    private $_meteostation;
    
    
    public function __construct($meteostation)
    {
        $this->_meteostation = $meteostation;
    }
    
    /**
     * Сбор данных с метеодатчика
     * @return app\models\dto\MeteoData
     */
    public function get()
    {
        return $this->_meteostation->getMeteoData();
    }
    
    /**
     * @return MeteoData
     */
    public function getHistory()
    {
        return $this->_meteostation->getMeteoDataHistory();
    }

    /**
     * @param app\models\dto\MeteoData $meteoData
     * @return boolean
     */
    public function save($meteoData)
    {
        $newMeteoData = new MeteoData;
        $newMeteoData->meteostation_id = $this->_meteostation->id;
        if (($newMeteoData->loadMeteoData($meteoData)) &&
            $newMeteoData->validate()
        ) {
            return $newMeteoData->save(false);
        }
    }
}