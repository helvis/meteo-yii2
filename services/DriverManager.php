<?php

namespace app\services;

use app\services\DriverLoader;

/**
 * Description of MeteostationManager
 *
 * @author helvis
 */
class DriverManager
{
    /**
     * @param app\models\tables\Meteostation $meteostation
     * return app\DriverInterface $driver
     */
    public function create($meteostation)
    {
        $DriverLoader = new DriverLoader;
        if (($classPath = $DriverLoader->genDriverClass($meteostation->type)) !== null) {
            return new $classPath(
                $meteostation->id
            );
        }
    }
}