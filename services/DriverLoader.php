<?php
/**
 *
 */

namespace app\services;

use Yii;
use yii\helpers\FileHelper;

/**
 * Класс управления библиотеками для разнызных типов метеостанций
 *
 * @author helvis
 */
class DriverLoader
{
    private $_driversDir;
    private $_drivers;


    public function __construct()
    {
        $this->_driversDir = isset(Yii::$app->params['drivers']) ? Yii::$app->params['drivers'] : 'drivers';
        $this->_drivers = $this->fetchDrivers();
    }

    /**
     * Возвращает список возможных библиотек-обработчиков,
     * @return array
     */
    public function getDrivers()
    {
        return $this->_drivers;
    }

    /**
     * генерирует полный namespace для переданного класса
     * @param string $driver
     * @return string|null
     */
    public function genDriverClass($driver)
    {
        $className = $driver;
        //Получение списка возможных классов
        $drivers = $this->getDrivers();
        //Проверка наличия среди них $className
        if (in_array($className, $drivers)) {
            //Если существует - формируем полный namespace
            $classPath = 'app\\' . $this->_driversDir . '\\' . $className;
            return $classPath;
        }
    }

    /**
     * определяет список возможных библиотек-обработчиков,
     * @return array
     */
    private function fetchDrivers()
    {
        $drivers = [];
        //Получение полного пути к каталогу библиотек-обработчиков
        $driversPath = Yii::$app->basePath . DIRECTORY_SEPARATOR . $this->_driversDir;
        //Получение списка файлов из каталога
        $files = FileHelper::findFiles($driversPath, ['only' => ['*.php']]);
        foreach($files as $file) {
            //Получение имени файла из его полного пути
            $meteoDriver = str_replace('.php', '', basename($file));
            //Добавление файла в массив, где $key = $value
            $drivers[$meteoDriver] = $meteoDriver;
        }
        return $drivers;
    }
}