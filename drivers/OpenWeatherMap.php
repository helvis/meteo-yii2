<?php
/**
 *
 */

namespace app\drivers;

use app\services\DriverInterface;
use app\models\dto\MeteoData;

/**
 * Получает данные виртуальных метеостанций от API http://openweathermap.org
 * Для работы модуля требуется php curl!
 * @author helvis
 */
class OpenWeatherMap implements DriverInterface
{
    /**
     * url для получения данных метеостанции от API по ID.
     * @var string
     */
    private $_urlGetById = 'http://api.openweathermap.org/data/2.5/station?';

    /**
     * Идентификатор метеостанции в базе OpenWeatherMap 
     * @var int 
     */
    private $_stationId;
    
    /**
     * Ключь для получения данных от API.
     * (Взят тут https://home.openweathermap.org/api_keys ).
     * @var string
     */
    private $_openWeatherKey = '3a1331d4f34ec944f2883b89767d19ac';

    /**
     * @var object curl
     */
    private $_connection = null;
    
    /**
     * Количество секунд ожидания при попытке соединения.
     * Используйте 0 для бесконечного ожидания. 
     * @var int 
     */
    private $_timeout = 2;

    /**
     * Количество попыток.
     * @var int
     */
    private $_retry = 3;

    /**
     * @var MeteoData
     */
    private $_meteoData;

    /**
     * @param int $stationId - Индентификатор метеостанции
     */
    public function __construct($stationId)
    {
        $this->_stationId = $stationId;
    }

    /**
     * @return MeteoData;
     */
    public function getMeteoData()
    {
        $this->curlInit();
        $this->curlConfig();
        while ($this->_retry) {
            if ($this->fetchStationData()) {
                $this->curlClose();
                return $this->_meteoData;
            }
            $this->_retry--;
        }
        $this->curlClose();
    }
    
    /**
     * Возвращает готовый url для получения данных от API
     * @return string
     */
    private function genUrl()
    {
        $query = [
            'id' => $this->_stationId,
            'APPID' => $this->_openWeatherKey
        ];
        return $this->_urlGetById . http_build_query($query);
               
    }

    /**
     * Иницализация CURL
     */
    private function curlInit()
    {
        //Инициализация сеанса cURL
        $this->_connection = curl_init();
    }

    /**
     * Начальная настройка CURL
     */
    private function curlConfig()
    {
        //Для возврата результата передачи в качестве строки
        curl_setopt($this->_connection, CURLOPT_RETURNTRANSFER, true);
        //Отключить SSL
        curl_setopt($this->_connection, CURLOPT_SSL_VERIFYPEER, false);
        //Установка поддерживаемого типа данных(json)
        curl_setopt($this->_connection, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        //Установка таймаута
        curl_setopt($this->_connection, CURLOPT_CONNECTTIMEOUT, $this->_timeout);
        //Установка загружаемого URL
        curl_setopt($this->_connection, CURLOPT_URL, $this->genUrl());
        var_dump($this->genUrl());
    }

    /**
     * Получение данных от API по ID,
     * конвертация из json
     * и разбирает методом parsingObject
     */
    private function fetchStationData()
    {
        //Попытка получения данных
        $content = json_decode(curl_exec($this->_connection));
        $statusCode = curl_getinfo($this->_connection, CURLINFO_HTTP_CODE);
        if ($statusCode != 200) {
            throw new \Exception($statusCode);
        }
        $data = $this->parsingObject($content);
        if ($this->parsingData($data)) {
            return true;
        }
    }

    private function curlClose()
    {
        //Закрытие соединения
        curl_close($this->_connection);
    }

    /**
     * Раскладывает многомерный объект или массив, в одномерный
     * @param stdClass $object
     * @param array $array
     * @return array
     */
    private function parsingObject($object, &$array = [])
    {
        foreach ($object as $key => $value) {
            if (is_object($value) || is_array($value)) {
                $this->parsingObject($value, $array);
            } else {
                $array[$key] = $value;
            }
        }
        return $array;
    }

    /**
     * @param array $data
     */
    private function parsingData(array $data)
    {
        $importantKeys = [
            'temp',
            'pressure',
            'humidity',
            'speed',
            'deg',
            'name',
            'lat',
            'lng',
        ];
        if ($this->arrayKeysExists($importantKeys, $data)) {
            $this->_meteoData = $this->parsingMeteoData($data);
            return true;
        }
    }
    
    /**
     * @param array $data
     * @return MeteoData
     */
    private function parsingMeteoData(array $data)
    {
        $meteoData = new MeteoData(
                $data['temp'],
                $data['pressure'],
                $data['humidity'],
                $data['speed'],
                $data['deg']
        );
        return $meteoData;
    }

    /**
     * Проверка наличия списка ключей в массиве
     * @param array $keys
     * @param array $array
     * @return boolean
     */
    private function arrayKeysExists(array $keys, array $array)
    {
        if ($keys === array_intersect($keys, array_keys($array))) {
            return true;
        }
        return false;
    }
}