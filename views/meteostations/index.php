<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список Метеостанций';
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a('Добавить метеостанцию', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'station_id',
        'location.name',
        'location.latitude',
        'location.longitude',
        'type',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>