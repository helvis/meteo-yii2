<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="news-item">
    <?= GridView::widget([
        'dataProvider' => $model,
        'columns' => [
            'created_at:datetime',
            'temperature',
            'pressure',
            'humidity',
            'windspeed',
            'winddirection',
        ],
    ]); ?>
</div>

