<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form app\models\forms\MeteostationForm */
/* @var $location app\models\tables\Location */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Метеостанция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meteostations-create">
    <?= $this->render('meteostation-form', [
            'meteostationForm' => $form,
            'types' => $types,
    ]) ?>
</div>
