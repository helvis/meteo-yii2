<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $meteostationForm app\models\forms\MeteostationForm */
$session = Yii::$app->session;
?>
<div class="meteostation-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php if ($session->hasFlash('ErrorForm')): ?>
            <?=Html::tag('div', $session->getFlash('ErrorForm'), ['class' => 'alert alert-danger']) ?>
        <?php endif ?>
        <?= $form->field($meteostationForm, 'station_id')->textInput($meteostationForm->scenario === 'update' ? ['disabled' => true] : []) ?>
        <?= $form->field($meteostationForm, 'name')->textInput($meteostationForm->scenario === 'update' ? ['disabled' => true] : []) ?>
        <?= $form->field($meteostationForm, 'latitude') ?>
        <?= $form->field($meteostationForm, 'longitude') ?>
        <?php 
            $params = [
                'prompt' => 'Выберите библиотеку...'
            ];
            echo $form->field($meteostationForm, 'type')->dropDownList($types, $params) ?>

        <div class="form-group">
            <div class="form-group">
            <?= 
                Html::submitButton(
                        $meteostationForm->scenario === 'update' ? 'Редактировать' : 'Создать', 
                        ['class' => $meteostationForm->scenario === 'update' ? 'btn btn-primary' : 'btn btn-success']
                ) 
            ?>
            <?= Html::a('Отмена', Yii::$app->request->referrer,['class' => 'btn btn-warning']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
