<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\tables\Meteostations */
/* @var $dataProvider yii\data\ActiveDataProvider */

$session = Yii::$app->session;
$this->title = $model->station_id;
$this->params['breadcrumbs'][] = ['label' => 'Метеостанция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meteostations-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить метеостанцию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'station_id',
            'location.name',
            'location.latitude',
            'location.longitude',
            'type',
            'updated_at:datetime',
        ],
    ]) ?>

    <h3>Метеоданные:</h3>
    <p>
        <?= Html::a('Получить новые данные', ['fetch', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php if ($session->hasFlash('ErrorFetchMeteoData')): ?>
        <?=Html::tag('div', $session->getFlash('ErrorFetchMeteoData'), ['class' => 'alert alert-danger']) ?>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-bordered table-hover',
        ],
        'columns' => [
            'created_at:datetime',
            'temperature',
            'pressure',
            'humidity',
            'windspeed',
            'winddirection',
        ],
    ]); ?>
</div>
