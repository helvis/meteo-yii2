<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form app\models\forms\MeteostationForm */
/* @var $location app\models\tables\Location */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Метеостанция', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $form->station_id, 'url' => ['view', 'id' => $form->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meteostations-update">
    <?= $this->render('meteostation-form', [
            'meteostationForm' => $form,
            'types' => $types,
    ]) ?>
</div>
