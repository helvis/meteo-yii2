<?php

namespace app\models;

use app\models\tables\Meteostation;
use yii\web\NotFoundHttpException;

/**
 * Description of MeteostationsRepository
 *
 * @author box
 */
class MeteostationsRepository
{
    /**
     * Удаляет станцию из репозитория
     * @param integer $id
     * @return boolean
     */
    public static function delete($id)
    {
        $meteostation = static::findById($id);
        return $meteostation->delete();
    }

    /**
     * Загружает модель одной метеостанции из хранилища
     * @param integer $id
     * @return Meteostations $meteostation загружаемая модель
     * @throws NotFoundHttpException если модель не найдена
     */
    public static function findById($id)
    {
        if (($meteostation = Meteostation::findOne($id)) !== null) {
            return $meteostation;
        }
        throw new NotFoundHttpException('Запрашиваемая страница не найдена');
    }

    /**
     * Отдает полный список метеостанций
     * @return Meteostations;
     */
    public static function getAll()
    {
        return Meteostation::find();
    }
}