<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\dto;

/**
 * Description of MeteoData
 * 
 * @property double $temperature
 * @property integer $pressure
 * @property integer $humidity
 * @property double $windspeed
 * @property integer $winddirection
 * 
 * @author box
 */
class MeteoData
{
    public $temperature;
    public $pressure;
    public $humidity;
    public $windspeed;
    public $winddirection;
    
    public function __construct(
            $temperature,
            $pressure,
            $humidity,
            $windspeed,
            $winddirection
    ) {
        $this->temperature = $temperature;
        $this->pressure = $pressure;
        $this->humidity = $humidity;
        $this->windspeed = $windspeed;
        $this->winddirection = $winddirection;
    }
}
