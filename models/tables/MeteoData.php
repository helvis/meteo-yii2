<?php
/**
 *
 */

namespace app\models\tables;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "meteodata".
 *
 * @property integer $id
 * @property integer $meteostation_id
 * @property double $temperature
 * @property integer $pressure
 * @property integer $humidity
 * @property double $windspeed
 * @property integer $winddirection
 * @property string $created_at
 *
 * @property Meteostations $meteostation
 */
class MeteoData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meteodata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meteostation_id', 'pressure', 'humidity', 'winddirection'], 'integer'],
            [['temperature', 'windspeed'], 'double'],
            [['created_at'], 'safe'],
            [['meteostation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meteostation::className(), 'targetAttribute' => ['meteostation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meteostation_id' => 'Meteostation ID',
            'temperature' => 'Температура °К',
            'pressure' => 'Давление мм.рт.ст.',
            'humidity' => 'Влажность %',
            'windspeed' => 'Скорость ветра м/с',
            'winddirection' => 'Направление ветра °',
            'created_at' => 'Время',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeteostation()
    {
        return $this->hasOne(Meteostation::className(), ['id' => 'meteostation_id']);
    }
    
    /**
     * 
     * @param MeteoData $meteoData
     * @return boolean
     */
    public function loadMeteoData(\app\models\dto\MeteoData $meteoData)
    {
        if (
            ($this->temperature = $meteoData->temperature) &&
            ($this->pressure = $meteoData->pressure) &&
            ($this->humidity = $meteoData->humidity) &&
            ($this->windspeed = $meteoData->windspeed) &&
            ($this->winddirection = $meteoData->winddirection)
        ) {
            return true;
        }
    }

    /* Поведение */
    
    /*
     * В этом методе содержаться различные поведения
     * 
     * Автоматически заполняет значения текущего времени для поля:
     * $this->created_at
     * $this->updated_at установлено для поведения как false(поле отсутсвует)
     * Если добалвляется новое значение - поле заполняется временем в UNIX формате
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(), //Поведение, заполняет значения текущего времени
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => false,
                ],
                'value' => function() {
                   return date('U'); //Функция возвращает значение времени в UNIX формате
                }
            ],
        ];
    }
    
}
