<?php

namespace app\models\tables;

use app\services\DriverManager;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "meteostations".
 *
 * @property integer $id
 * @property string $type
 * @property integer $station_id
 * @property integer $updated_at
 * @property integer $created_at
 * @property Location[] $location
 * @property Meteodata[] $meteodatas
 */
class Meteostation extends ActiveRecord
{
    /**
     * @var app\DriverInterface;
     */
    private $_driver;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meteostations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'station_id'], 'required', 'message' => 'Поле обязательно для заполнения'],
            [['station_id', 'updated_at', 'created_at'], 'integer'],
            ['type', 'string'],
            [['station_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'type' => 'Библиотека обработчик',
            'station_id' => 'Идентификатор',
            'updated_at' => 'Обновлено',
            'created_at' => 'Создано',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['meteostation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeteoDataHistory()
    {
        return $this->hasMany(MeteoData::className(), ['meteostation_id' => 'id']);
    }

    /**
     * @return \app\models\dto\MeteoData
     */
    public function getMeteoData()
    {
        return $this->_driver->getMeteoData();
    }

    public function afterFind()
    {
        $this->_driver = DriverManager::create($this);
    }

    /* Поведение */
    
    /*
     * В этом методе содержаться различные поведения
     * 
     * Автоматически заполняет значения текущего времени для полей:
     * $this->created_at и $this->updated_at
     * Если добалвляется новый пользователь - $this->created_at
     * Если редактируется - $this->updated_at
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className() //Поведение, заполняет значения текущего времени
        ];
    }
}
