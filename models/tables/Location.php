<?php
/**
 *
 */

namespace app\models\tables;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property integer $meteostation_id
 * @property string $name
 * @property double $latitude
 * @property double $longitude
 *
 * @property Meteostations $meteostation
 */
class Location extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'latitude', 'longitude'], 'required', 'message' => 'Поле обязательно для заполнения'],
            [['meteostation_id'], 'integer'],
            [['latitude', 'longitude'], 'double'],
            [['name'], 'string', 'max' => 255],
            ['name', 'unique'],
            [['meteostation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meteostation::className(), 'targetAttribute' => ['meteostation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meteostation_id' => 'Meteostation ID',
            'name' => 'Название(расположение)',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeteostation()
    {
        return $this->hasOne(Meteostation::className(), ['id' => 'meteostation_id']);
    }
}
