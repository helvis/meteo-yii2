<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\forms;

use app\models\tables\Meteostation;
use app\models\tables\Location;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Description of MeteostationForm
 *
 * @author helvis
 */
class MeteostationForm extends Model
{
    public $id;
    public $station_id;
    public $name;
    public $latitude;
    public $longitude;
    public $type;
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['station_id', 'name','type', 'latitude', 'longitude', 'required'];
        $scenarios['update'] = ['type', 'latitude', 'longitude', 'required'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 255],
            [
                ['type', 'station_id', 'name', 'latitude', 'longitude'],
                'required',
                'message' => 'Поле обязательно для заполнения'
            ],
            [['id', 'station_id'], 'integer'],
            [['latitude', 'longitude'], 'double'],
            ['type', 'string'],
            [
                'station_id', 'unique',
                'targetClass' => Meteostation::className(),
                'message' => 'Этот идентификатор уже занят'
            ],
            [
                'name', 'unique',
                'targetClass' => Location::className(),
                'message' => 'Это имя уже занято'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'station_id' => 'Идентификатор',
            'type' => 'Библиотека обработчик',
            'name' => 'Название(расположение)',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
        ];
    }
    
    public function loadMeteostation($model)
    {
        if (
            ($this->id = $model->id) &&
            ($this->station_id = $model->station_id) &&
            ($this->type = $model->type) &&
            ($this->name = $model->location->name) &&
            ($this->latitude = $model->location->latitude) &&
            ($this->longitude = $model->location->longitude)
        ) {
            return true;
        }
    }
}