<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meteostations`.
 */
class m161228_201201_create_meteostations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('meteostations', [
            'id' => $this->primaryKey(),
            'station_id' => $this->integer()->notNull()->unique(),
            'type' => $this->string()->notNull(),
            'updated_at' => $this->integer(11),
            'created_at' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('meteostations');
    }
}
