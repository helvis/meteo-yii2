<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meteodata`.
 */
class m161228_201213_create_meteodata_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('meteodata', [
            'id' => $this->primaryKey(),
            'meteostation_id' => $this->integer(),
            'temperature' => $this->float(),
            'pressure' => $this->integer(),
            'humidity' => $this->integer(),
            'windspeed' => $this->float(),
            'winddirection' => $this->integer(),
//            'created_at' => $this->timestamp(),
            'created_at' => $this->integer(11),
        ]);
        $this->addForeignKey(
            'meteodata_meteostations_id',
            'meteodata',
            'meteostation_id',
            'meteostations',
            'id',
            'CASCADE'
            );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('meteodata');
    }
}
