<?php

use yii\db\Migration;

/**
 * Handles the creation of table `location`.
 */
class m161228_201226_create_location_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('location', [
            'id' => $this->primaryKey(),
            'meteostation_id' => $this->integer(),
            'name' => $this->string(),
            'latitude' => $this->float(),
            'longitude' => $this->float(),
        ]);
        $this->addForeignKey(
            'location_meteostations_id',
            'location',
            'meteostation_id',
            'meteostations',
            'id',
            'CASCADE'
            );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('location');
    }
}
