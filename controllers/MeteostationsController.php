<?php
/**
 *
 */

namespace app\controllers;

use app\models\MeteostationsRepository;
use app\models\forms\MeteostationForm;
use app\services\DriverLoader;
use app\services\MeteostationService;
use app\services\MeteoDataService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * MeteostationsController содержит действия
 * создания, удаления, изменения и отображения данных модели Meteostations
 */
class MeteostationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Действие выводит список всех зарегистрированных метеостанций
     * @return mixed
     */
    public function actionIndex()
    {
        $query = MeteostationsRepository::getAll();
        $dataProvider = $this->getDataProvider($query);
        $dataProvider->sort = ['defaultOrder' => ['station_id' => SORT_ASC]];
        //Вывод представления
        return $this->render('index',
            [
                'dataProvider' => $dataProvider
            ]
        );
    }

    /**
     * Выводит конфигурацию и данные одной метеостанции
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = MeteostationsRepository::findById($id);
        $MeteoDataService = new MeteoDataService($model);
        //Создание провайдера данных для коллекции MeteoData
        $query = $MeteoDataService->getHistory();
        $dataProvider = $this->getDataProvider($query);
        $dataProvider->sort = ['defaultOrder' => ['created_at' => SORT_DESC]];
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создает новую модель Meteostations.
     * Если создание успешно, выполняется переадресация в действие view созданной модели
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new MeteostationForm;
        $form->scenario = 'create';

        if (Yii::$app->request->post()) {
            if (
                $form->load(Yii::$app->request->post()) &&
                $form->validate() &&
                $model = MeteostationService::create($form)
            ) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            //Создание в сессии сообщения об ошибке
            Yii::$app->session->setFlash('ErrorForm',
                'При регистрации новой метеостанции возникла ошибка.'
            );
        }
        //Получение списка возможных обработчиков
        $driverLoader = new DriverLoader;
        $drivers = $driverLoader->getDrivers();
        return $this->render('create', [
            'form' => $form,
            'types' => $drivers,
        ]);
    }

    /**
     * Обновляет существующую модель Meteostations.
     * Если обновление успешно, выполняется переадресация в действие view созданной модели
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = MeteostationsRepository::findById($id);
        $form = new MeteostationForm;
        $form->scenario = 'update';
        if (Yii::$app->request->post()) {
            if (
                $form->load(Yii::$app->request->post()) &&
                $form->validate() &&
                MeteostationService::update($form, $model)
            ) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            Yii::$app->session->setFlash('ErrorForm',
                'При редактировании метеостанции возникла ошибка.'
            );
        }
        $form->loadMeteostation($model);
        //Получение списка возможных обработчиков
        $driverLoader = new DriverLoader;
        $drivers = $driverLoader->getDrivers();
        return $this->render('update', [
            'form' => $form,
            'types' => $drivers,
        ]);
    }

    /**
     * Удаляет существующую метеостанцию.
     * Если удаление успешно - выполняется переадресация в действие index
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        MeteostationsRepository::delete($id);
        return $this->redirect(['index']);
    }

    /**
     * Получает данные от метеодатчика,
     * и последующая запись их в базу
     * @param integer $id
     * @return mixed
     */
    public function actionFetch($id)
    {
        $model = MeteostationsRepository::findById($id);
        $MeteoDataService = new MeteoDataService($model);
        try {
            //Получаем новые данные от метеодатчика
            $meteoData = $MeteoDataService->get();
            //Выполняем сохранение
            if (!$MeteoDataService->save($meteoData)) {
                //иначе выводим сообщение об ошибке
                Yii::$app->session->setFlash(
                    'ErrorFetchMeteoData',
                    'Ошибка сохранения данных.'
                );
            }
        } catch (\Exception $error) {
            //иначе выводим сообщение об ошибке
            Yii::$app->session->setFlash(
                    'ErrorFetchMeteoData',
                    'Ошибка получения метеоданных: ' . $error
            );
        }
        //Переадресация на страницу просмотра данных метеостанции
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Возвращает провайдер данных для полученной модели
     * @param Meteostation|MeteoData $query
     * @return ActiveDataProvider
     */
    protected function getDataProvider($query, $pageSize = 10)
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
    }
}
