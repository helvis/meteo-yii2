Приложение для сбора данных с метеостанций
============================

Приложение выполняет сбор данных с датчиков метеостанций, с последующим сохранением в базу,
и выводом этих данных на экран.

[Cкачать](https://bitbucket.org/helvis/meteo-yii2)

Структура
-------------------
      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


Требования
------------

PHP 5.4.0.

PHP CURL

MySQL Server - ?Уточнить минимальную версию?


Установка
------------

### Установка с помощью Git + Composer

Если у вас нет Git и Composer, скачайте их:
[Git](https://git-scm.com/downloads)
[Composer](https://getcomposer.org/download/)
~~~
git clone https://bitbucket.org/helvis/meteo-yii2 meteo-yii2
cd meteo-yii2
composer install
~~~
В настройках вашего веб сервера, пропишите корневым для сайта каталог:
~~~
 meteo-yii2/web
~~~
Назначьте для каталогов `/runtime`, `/web/assets` и файлу `yii` права `0777`
~~~
chmod 0777 -R runtime
chmod 0777 -R web/assets
chmod 0777 yii
~~~

### Установка из архива пока не предусмотрена


Настройка
-------------

### Database

Отредактируйте файл `/config/db.php`, для корректного подключения к базе данных.
```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

**Заметка:**
Указанная база (например `yii2basic`) должна быть создана вами самостоятельно.

### Миграции
Для создания всех необходимых таблиц, требуется выполнить в командной строке следующие команду:
~~~
php yii migrate
~~~

Добавление новых типов метеостанций
-------------
По умолчанию в приложении предусмотрено добавление только виртуальных метеостанций,
взаимодействующий по API [OpenWeatherMap](http://openweathermap.org/api_station), библиотека `/types/OpenWeatherMap.php`


Для добавления новых типов станций, требуется реализация библиотеки обработчика, которая должна:

* Представлять собой ***php Class***, реализующий ***MeteostationInterface*** (смотри файл `/models/types/MeteostationInterface.php`)

* Находиться в каталоге `/types`

* Стиль кода соответствовать стандарту кодирования psr-2(для корректной автозагрузки)



Новый тип метеостанции будет доступен в соответствующем выпадающем меню `Тип`, в форме создания новой метеостанции.

